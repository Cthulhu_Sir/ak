const express = require('express');
const app = express();
require('dotenv').config();
const ejs = require('ejs-locals');
const path = require('path');

const port = 3000;

const router = require('./modules/express-router-module');

app.use('/', router);

app.engine('ejs', ejs);
app.set('views', path.join(__dirname, 'pages'));
app.set('view engine', 'ejs');
app.use('/assets', express.static(__dirname + '/pages/assets'));

app.listen(port, (err) => {
   if (err){
       return console.log('Some error occured: ', err);
   }

   console.log(`Server is listening on port ${port}`);
});