const mysql = require('mysql');

const connection = mysql.createConnection({
    host: "good-coffee-near-you.cgcnnlvq1rjd.us-east-1.rds.amazonaws.com",
    port: "3306",
    database: "good_coffee_near_you",
    user: "barista",
    password: "good0afternoon"
    // stringifyObjects: true
});

connection.connect(function (err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }

    console.log('connected as id ' + connection.threadId);
});

function getAllCoffeeHouses() {
    return connection.query("SELECT * FROM coffee_houses", (error, results, fields) => {
        return results
    });
}

function getUser(email, callback) {
    try {
        return connection.query("select * from user where email like ?", [email], (error, results, fields) => {
            if (error) throw error;
            // console.log(results[0]);
            callback(results[0]);
        })
    } catch (e) {
        return "error occurred"
    }
    // return ({err: "Nothing here"})
}

function addUser(email, password, callback) {
    try {
        return connection.query("INSERT INTO user (email, password) VALUES (?, ?)", [email, password], (error, results, fields) => {
            if (error) throw error;
            // console.log(results);
            callback(results);
        })
    } catch {
        return "error occured"
    }
    // return ({err: "Nothing here"})
}

function getCoffeeHouses(data, callback) {
    console.log(data);
    try {
        return connection.query("select ch.coffee_house_id, ch.name, a.address, chm.total_rating, a.latitude, \n" +
            "a.longitude, chm.coffee_id, chm.cost, ch.has_food_to_go\n" +
            "FROM coffee_house ch JOIN coffee_house_address cha on ch.coffee_house_id = cha.coffee_house_id\n" +
            "JOIN address a on cha.address_id = a.address_id \n" +
            "JOIN coffee_house_menu chm on chm.coffee_house_id = ch.coffee_house_id\n" +
            // "LEFT JOIN user_rating ur on ch.coffee_house_id = ur.coffee_house_id and chm.coffee_id = ur.coffee_id " +
            // "LEFT JOIN user u on ur.user_id = u.user_id " +
            "WHERE " +
            // "(sqrt((a.latitude-?)^2 + a.longitude-?)^2) < ?\n" +
            // "and "+
            // "u.email like ? and " +
            "chm.coffee_id=? and chm.total_rating >= ?\n" +
            "and chm.cost between ? and ?", [data.coffee, data.rating, 0, data.price], function (error, resutls, fields) {
            if (error) throw error;
            // console.log(resutls);
            callback(resutls);
        })
    } catch (e) {
        return "error occurred"
    }
}

function insertUserScore(data, callback) {
    console.log("inserting...");
    try {
        return connection.query("INSERT INTO user_rating VALUES " +
            "((SELECT user_id FROM user WHERE email like ?), ?, ?, ?);", [data.email, data.coffee_house_id, data.coffee_id, data.value], (error, results, fields) => {
            // console.log(error);
        })
    } catch (e) {
        return "error occurred"
    }
}

function selectUserScore(data, callback) {
    console.log("selecting...");
    try {
        return connection.query("SELECT rating FROM user_rating WHERE user_id=? AND coffee_house_id like ? AND coffee_id=?",
            [data.user_id, data.coffee_house_id, data.coffee_id], (error, results, fields) => {
                console.log(results);
                callback(results[0]);
            })
    } catch (e) {
        return "error occurred"
    }
}

function updateUserScore(data, callback) {
    console.log("updating...");
    try {
        return connection.query("UPDATE user_rating SET rating=? WHERE user_id=? AND coffee_id=? AND coffee_house_id like ? ESCAPE '#'",
            [data.value, data.user_id, data.coffee_id, data.coffee_house_id], (error, results, fields) => {
                callback(results);
            })
    } catch (e) {
        return "error occurred"
    }
}

function selectTotalRating(data, callback) {
    console.log("selecting...");
    try {
        return connection.query("SELECT total_rating from coffee_house_menu WHERE coffee_id=? AND coffee_house_id like ?",
            [data.coffee_id, data.coffee_house_id], (error, results, fields) => {
                console.log(results);
                callback(results);
            })
    } catch (e) {
        return "error occurred"
    }
}

function addNewPlace(data, callback) {
    console.log("adding new place...");
    try {
        return connection.query("INSERT INTO form_new_place VALUES (?, ? , ?, ?, ?, ?, ?, ?, ?, ?)", [data.name, parseInt(data.phone), data.website, parseFloat(data['latte-cost']), parseFloat(data['cappuccino-cost']), parseFloat(data['americano-cost']), data.address, data.lat, data.lng, parseInt(data['food-to-go'])], (error, results, fields) =>{
            if(error){
                callback({err: error});
            }else{
                callback(results);
            }
        })
    } catch (e) {
        console.error(e.stack);
    }
}

function addNewPrice(data, callback) {
    console.log("adding new place...");
    try {
        return connection.query("INSERT INTO form_new_price VALUES (?, ? , ?, ?, ?, ?, ?)", [data.name, parseInt(data.phone), data.website, parseFloat(data['latte-cost']), parseFloat(data['cappuccino-cost']), parseFloat(data['americano-cost']), parseInt(data['food-to-go'])], (error, results, fields) =>{
            if(error){
                callback({err: error});
            }else{
                callback(results);
            }
        })
    } catch (e) {
        console.error(e.stack);
    }
}

module.exports = {
    connection,
    getCoffeeHouses,
    getUser,
    addUser,
    insertUserScore,
    selectUserScore,
    updateUserScore,
    selectTotalRating,
    addNewPlace,
    addNewPrice
};


