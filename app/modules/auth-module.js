const bcrypt = require('bcrypt');

function encrypt(password){

    const salt = bcrypt.genSaltSync(10);
    // console.log(password, salt);
    return bcrypt.hashSync(password, salt);
}

function compare(pass1, pass2){
    return bcrypt.compareSync(pass1, pass2);
}

module.exports = {
  encrypt,
  compare
};