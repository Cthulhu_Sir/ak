const express = require('express');
const router = express.Router();
const cookieParser = require('cookie-parser');

require('dotenv').config();

const dao = require('./data-access-module');
const auth = require('./auth-module');

const session = require('express-session');
const dbSession = require('express-mysql-session')(session);

router.use(express.urlencoded({extended: true}));
router.use(cookieParser());

const sessionStore = new dbSession({
    host: "good-coffee-near-you.cgcnnlvq1rjd.us-east-1.rds.amazonaws.com",
    port: "3306",
    database: "good_coffee_near_you",
    user: "barista",
    password: "good0afternoon",
    schema: {
        tableName: 'sessions',
        columnNames: {
            session_id: 'session_id',
            expires: 'expires',
            data: 'data'
        }
    }
});
router.use(session({
    store: sessionStore,
    secret: 'tis_is_secret'
}));

router.get('/', (request, response) => {
    // console.log(request.session);
    // request.session.something = "123";
    response.render('mainPage', {
        res: request.session.email
    })
});

router.post('/login', (request, response) => {
    if (!request.session.email) {
        console.log(request.body);

        let data = dao.getUser(request.body.email, (res) => {
                if (auth.compare(request.body.password, res.password)) {
                    request.session.email = request.body.email;
                    request.session.user_id = res.user_id;
                    response.redirect('/');
                } else {
                    response.redirect('/');
                }
            }
        );


    }
})
;

router.post('/registration', (request, response) => {
    if (!request.session.email) {
        // console.log(request.body);

        let correct = true;

        if (request.body.email === '') {
            correct = false;
        }
        if (request.body.password === '') {
            correct = false;
        }

        if (correct) { //Сделать проверки на корректность данных <---------------------------------------------------------
            let data = dao.addUser(request.body.email, auth.encrypt(request.body.password), (res) => {
                response.email = data.email;
                response.password = request.body.password;
                response.redirect('/login');
            });
        }
    }
});

function dataCorrection(data) {
    // console.log(data);
    if (data.coffee === 'cappuccino') {
        data.coffee = 2;
    }
    if (data.coffee === 'latte') {
        data.coffee = 1;
    }
    if (data.coffee === 'americano') {
        data.coffee = 3;
    }
    if (data.coffee !== 1 && data.coffee !== 2 && data.coffee !== 3) {
        data.coffee = 1;
    }
    if (typeof data.price !== typeof 1) {
        data.price = parseInt(data.price);
    }

    if (data.price < 0 || isNaN(data.price)) {
        data.price = 999999;
    }
    if (typeof data.rating !== typeof 1) {
        data.rating = parseInt(data.rating);
    }

    if (data.rating < 0 || isNaN(data.rating)) {
        data.rating = 0;
    }
    if (data.rating > 5) {
        data.rating = 5;
    }
    if (typeof data.foodToGo !== typeof true) {
        data.foodToGo = false;
    }
    if (data.foodToGo === false) {
        data.foodToGo = 0;
    }
    if (data.foodToGo === true) {
        data.foodToGo = 1;
    }

    return data;
}

router.post('/getCoffeeHouses', (request, response) => {
    //Check data fields
    // console.log(request.body);
    let req;
    try {
        req = dataCorrection(request.body.data);
    } catch (e) {
        response.json("Whoops!");
        return;
    }

    req.email = request.session.email;

    dao.getCoffeeHouses(req, (data) => {
        if (!data.err) {
            // console.log(data);
            console.log(request.session.user_id);
            response.json({
                res: data,
                user_id: request.session.user_id
            });
        } else {
            response.json({err: data.err});
        }
    });

});

router.get('/logout', (request, response) => {
    request.session.destroy();
    response.redirect('/')
});

router.post('/addUSerScore', (request, response) => {
    if (request.session.email) {
        request.body.email = request.session.email;
        dao.insertUserScore(request.body, (res) => {

        });
    } else {
        response.render('/');
    }
});

router.post('/getUserScore', (request, response) => {
    if (request.session.email) {
        dao.selectUserScore(request.body, (res) => {
            response.json({res: res});
        })
    }
});

router.post('/updateUserScore', (request, response) => {
    if (request.session.email) {

        request.body.user_id = request.session.user_id;
        dao.updateUserScore(request.body, (res) => {
            response.json({res: res});
        });
    }
});

//Тут принимаем его
router.post('/selectTotalRating', (request, response) => {
    if (request.session.email) {

        request.body.user_id = request.session.user_id;
        dao.selectTotalRating(request.body, (res) => {
            response.json({res: res});
        });
    }
});

router.get('/addNewPlaceForm',(request, response) => {
    response.render('addNewPlace');
});

router.post('/addNewPlace', (request, response) => {
    if(request.body['food-to-go'] === undefined){
        request.body['food-to-go'] = 0
    } else {
        request.body['food-to-go'] = 1;
    }
    // request.body['food-to-go'] = request.body['food-to-go'] !== undefined;
    console.log(request.body);
    try {
        dao.addNewPlace(request.body, (res)=>{
            if(res.err){
                response.redirect('/error');
            } else {
                response.redirect('/done');
            }
        })
    } catch (e) {
        console.error(e.stack);
    }
});

router.get('/addNewCoffeePriceForm', (request, response) => {
    response.render('addNewCoffeePrice');
});

router.post('/addNewCoffeePrice', (request, response) => {

    if(request.body['food-to-go'] === undefined){
        request.body['food-to-go'] = 0
    } else {
        request.body['food-to-go'] = 1;
    }
    // request.body['food-to-go'] = request.body['food-to-go'] !== undefined;
    console.log(request.body);
    try {
        dao.addNewPrice(request.body, (res)=>{
            if(res.err){
                response.redirect('/error');
            } else {
                response.redirect('/done');
            }
        })
    } catch (e) {
        console.error(e.stack);
    }
});

router.get('/error', (request, response) => {
    response.render('error');
});

router.get('/done', (request, response) => {
    response.render('done');
});
// dao.connection.destroy();

module.exports = router;

