let userPos = {};

let map;

let markers = [];

// Initialize and add the map
function initMap() {
    // The location of Uluru
    let eltech = {lat: 59.972022, lng: 30.320558};
    // The map, centered at Uluru
    map = new google.maps.Map(
        document.getElementById('map'), {zoom: 4});

    //Geolocation
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            let pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            userPos = pos;

            // infoWindow.setPosition(pos);
            // infoWindow.setContent('Location found.');
            // infoWindow.open(map);
            // addUserMarker(pos);
        }, function () {
            userPos = eltech;
            // addUserMarker(eltech);
            // handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {

        // Browser doesn't support Geolocation
        // handleLocationError(false, infoWindow, map.getCenter());
        userPos = eltech;
        // addUserMarker(eltech);
    }
}

function addUserMarker(pos) {
    map.setCenter(pos);
    map.setZoom(16);
    let marker = new google.maps.Marker({
        position: pos,
        map: map,
        animation: google.maps.Animation.DROP,
        icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
    })
    markers.push(marker);
}

function resizeMap() {
    $('#map-container').attr('style', `height: ${$(window).height() - 50}px; width: ${$(window).width() - 10}px;`)
}

function addNewMarker(res) {

    console.log(res);
    console.log(typeof res);
    res.res.forEach((row, i) => {
        setTimeout(function () { //ставим таймаут, чтобы не отрисовывать миллиард маркеров за один раз

            let rating = row.total_rating;
            let ftg;
            if (row.foodToGo) {
                ftg = "Есть еда с собой"
            } else {
                ftg = "Нет еды с собой"
            }

            let coffeeName;
            switch (row.coffee_id) {
                case 1: coffeeName = "Латте"; break;
                case 2: coffeeName = "Капучино"; break;
                case 3: coffeeName = "Американо"; break;
            }

            let contentString = `<div class="card" style="border: 0">
                                      <!--<img class="card-img-top" src=".../100px180/?text=Image cap" alt="Card image cap">-->
                                      <div class="card-body" style="width: 100%">
                                        <h5 class="card-title">${row.name}</h5>
                                      </div>
                                      <div class="card-body">
                                    
                                        <p>${coffeeName} стоит ${row.cost}р</p>
                                       
                                        <p id="${row.coffee_house_id}-rating-p">Рейтинг: ${rating}</p>
                                        
                                        <p>Есть еда с собой</p>
                                      
                                        <p>
                                          <a class="na-link" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="getRating('${row.coffee_house_id}','${row.coffee_id}', '${res.user_id}')">
                                                оценить
                                          </a>
                                        </p>
                                          <div class="collapse" id="collapseExample">
                                                <div class="input-group mb-3">
                                                  <input id="${row.coffee_house_id}-rating" value="${rating}" type="number" min="1" max="5" class="form-control " placeholder="${rating}" aria-label="rate" aria-describedby="button-addon2">
                                                  <div class="input-group-append">
                                                    <button class="btn btn-outline-secondary" type="button" onclick="addRating('${row.coffee_house_id}', '${row.coffee_id}')">+</button>
                                                  </div>
                                                </div>
                                          </div>  
                                      </div>
                                      <div class="card-body">
                                      </div>
                                 </div>`;// Строка с HTML для InfoWindow

            let infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            let mp = {
                lat: row.latitude,
                lng: row.longitude
            };

            let marker = new google.maps.Marker({
                position: mp,
                map: map,
                animation: google.maps.Animation.DROP
            });

            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });
            markers.push(marker);
        }, i * 50);

    });
}

function getRating(coffee_house_id, coffee_id, user_id){
    $.ajax({
        url: 'getUserScore',
        dataType: 'json',
        type: 'post',
        data: {
            coffee_house_id: coffee_house_id,
            coffee_id: coffee_id,
            user_id: user_id
        },
        success: function(data) {
            console.log(data);
            if(data.res === undefined){
                $('#'+ coffee_house_id + '-rating').addClass('common');
            } else {
                // console.log(data.res.rating);
                $('#' + coffee_house_id + '-rating').val(data.res.rating)
            }
        },
        error: function (err) {

        }
    })
}

function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

function addRating(id, coffee_id) {
    let rv = $('#' + id + '-rating').val();
    console.log(id, " ", coffee_id, " ", rv);

    let url;
    if($('#' + id + '-rating').hasClass('common')){
        url = 'addUserScore'
    } else {
        url = 'updateUserScore'
    }

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: {
            value: rv,
            coffee_house_id: id,
            coffee_id: coffee_id
        },
        success: (res) => {
            if($('#' + id + '-rating').hasClass('common')){
                $('#'+id+'-rating').removeClass('common');
            }
            $.ajax({
                url: 'selectTotalRating',//мяу
                type: 'POST',
                dataType: 'json',
                data: {
                    coffee_house_id: id,
                    coffee_id: coffee_id
                },
                success: (res) => {
                    // console.log(res.res[0].total_rating);
                    $('#'+id+'-rating-p').html('Рейтинг: ' + res.res[0].total_rating);
                }
            });
            // alert('Спасибо за оценку!')
        },
        error: (err) => {

        }
    })
}

function askForCoffeeHouses() {

    let data = {};

    data.coffee = getActiveCoffee();
    data.price = getMaxPrice();
    data.rating = getMinRating();
    data.foodToGo = getFoodToGo();
    data.pos = userPos;

    console.log(data);

    $.ajax({
        url: 'getCoffeeHouses',
        type: 'POST',
        dataType: 'json',
        data: {data: data},
        success: (res) => {
            if (!res.err) {
                setMapOnAll(null);
                markers = [];
                addUserMarker(userPos);
                addNewMarker(res);
            } else {
                console.log(res.err);
            }
        },
        error: (err) => {

        }
    });
}

function getActiveCoffee() {
    if ($('#coffee-cappuccino').parent().hasClass('active'))
        return 'cappuccino';
    if ($('#coffee-latte').parent().hasClass('active'))
        return 'latte';
    if ($('#coffee-americano').parent().hasClass('active'))
        return 'americano';
}

function getMaxPrice() {
    if ($('#input-price').val() !== '') {
        return $('#input-price').val();
    }
    return undefined;
}

function getMinRating() {
    if ($('#input-rating').val() !== '') {
        return ($('#input-rating').val());
    } else {
        return undefined;
    }
}

function getFoodToGo() {
    return $('#input-food-to-go').parent().hasClass('checked');
}

function filter() {
    console.log($('#coffee-cappuccino').parent());
    console.log("cappuccino: ", $('#coffee-cappuccino').parent().hasClass('active'));
    console.log("latte: ", $('#coffee-latte').parent().hasClass('active'));
    console.log("americano: ", $('#coffee-americano').parent().hasClass('active'));
    console.log("price: ", $('#input-price').val());
    console.log("rating: ", $('#input-rating').val());
    console.log("food-to-go: ", $('#input-food-to-go').parent().hasClass('checked'));

    askForCoffeeHouses()

}

function check() {
    let target = $('#input-food-to-go').parent()
    let chk = target.hasClass('checked');
    if (chk) {
        target.removeClass('checked');
    } else {
        target.addClass('checked');
    }
}

$(window).load(resizeMap);
$(window).load(askForCoffeeHouses);
window.addEventListener('resize', resizeMap);