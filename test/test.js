const assert = require('assert');
const chai = require('chai');
const expect = chai.expect;

var describe = require('mocha').describe;
var pre = require('mocha').before;
var it = require('mocha').it;

const dao = require('../app/modules/data-access-module');

describe('DBC', function () {
    describe('#getUser(email)', function () {
        it('should reply with undefined when the value is not present', async function () {
            let res = await dao.getUser('abc', (res) => {
                console.log(res);
                expect(res).to.be.an(undefined);
            });
        });
        it('should reply with user info when the value is present', async function () {
            let param = 'somemail@mail.ru';
            let res = await dao.getUser(param, (res) => {
                console.log(res);
                expect(res).to.have.propety('email', {email: param});
            });
        })
    });
});